//
//  ContactCardCell.swift
//  Example-Storyboard
//
//  Created by aleksander.lorenc on 30/10/2019.
//  Copyright © 2019 Unwrapped Software. All rights reserved.
//

import UIKit

class ContactCardCell: UITableViewCell {
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var addressLabel: UILabel!
    @IBOutlet private var phoneNumberLabel: UILabel!
    @IBOutlet private var photoImageView: UIImageView!

    func configure(name: String,
                   address: String,
                   phoneNumber: String,
                   photo: UIImage) {
        nameLabel.text = name
        addressLabel.text = address
        phoneNumberLabel.text = phoneNumber
        photoImageView.image = photo
    }
}
