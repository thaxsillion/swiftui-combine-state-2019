//
//  ViewController.swift
//  Example-Code
//
//  Created by aleksander.lorenc on 30/10/2019.
//  Copyright © 2019 Unwrapped Software. All rights reserved.
//

import UIKit

struct Contact {
    let firstName: String
    let lastName: String
    let phoneNumber: String
    let address: String
    let photo: String
}

private var contacts: [Contact] = [
    Contact(firstName: "Rhea",
            lastName: "Poole",
            phoneNumber: "+12 345 567 890",
            address: "Sunbay 2173",
            photo: "photo"),
    Contact(firstName: "Seren",
        lastName: "Murillo",
        phoneNumber: "+12 345 567 890",
        address: "Sunbay 2173",
        photo: "photo2"),
    Contact(firstName: "Ivor",
        lastName: "Adams",
        phoneNumber: "+12 345 567 890",
        address: "Sunbay 2173",
        photo: "photo3"),
    Contact(firstName: "Rhea",
            lastName: "Poole",
            phoneNumber: "+12 345 567 890",
            address: "Sunbay 2173",
            photo: "photo"),
    Contact(firstName: "Seren",
        lastName: "Murillo",
        phoneNumber: "+12 345 567 890",
        address: "Sunbay 2173",
        photo: "photo2"),
    Contact(firstName: "Ivor",
        lastName: "Adams",
        phoneNumber: "+12 345 567 890",
        address: "Sunbay 2173",
        photo: "photo3")
]

class ViewController: UIViewController {
    private let tableView = UITableView()

    init() {
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        view = tableView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(ContactCardCell.self, forCellReuseIdentifier: "ContactCardCell")
        tableView.dataSource = self
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCardCell", for: indexPath) as? ContactCardCell else {
            fatalError("Could not dequeue cell")
        }

        let contact = contacts[indexPath.row]
        cell.configure(name: "\(contact.firstName) \(contact.lastName)",
                       address: contact.address,
                       phoneNumber: contact.phoneNumber,
                       photo: UIImage(named: contact.photo)!)

        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        contacts.count
    }
}
