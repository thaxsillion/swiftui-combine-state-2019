//
//  ContactCardCell.swift
//  Example-Code
//
//  Created by aleksander.lorenc on 30/10/2019.
//  Copyright © 2019 Unwrapped Software. All rights reserved.
//

import UIKit
import Cartography

class ContactCardCell: UITableViewCell {
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 17)
        return label
    }()

    private lazy var addressLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: .light)
        label.textColor = .darkGray
        return label
    }()

    private lazy var phoneNumberLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor(white: 60 / 255.0, alpha: 1.0)
        return label
    }()

    private lazy var photoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addViews()
        configureViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func addViews() {
        addSubview(nameLabel)
        addSubview(addressLabel)
        addSubview(phoneNumberLabel)
        addSubview(photoImageView)
    }

    private func configureViews() {
        constrain(photoImageView) { view in
            view.width == 50
            view.height == 50
        }

        constrain(photoImageView, self, nameLabel, addressLabel, phoneNumberLabel) {
            photo, view, name, address, phoneNumber in
            align(top: name, phoneNumber, photo)
            photo.left == view.left + 16
            photo.top == view.top + 16
            photo.bottom == view.bottom - 16 ~ .defaultHigh
            name.left == photo.right + 16
            address.left == name.left
            address.top == name.bottom + 4
            phoneNumber.right == view.right - 16
        }
    }

    func configure(name: String,
                   address: String,
                   phoneNumber: String,
                   photo: UIImage) {
        nameLabel.text = name
        addressLabel.text = address
        phoneNumberLabel.text = phoneNumber
        photoImageView.image = photo
    }
}
