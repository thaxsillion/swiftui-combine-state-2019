//
//  Contacts.swift
//  Example-SwiftUI-Finished
//
//  Created by aleksander.lorenc on 30/10/2019.
//  Copyright © 2019 Unwrapped Software. All rights reserved.
//

import Foundation

struct Contact: Identifiable {
    var id: UUID = UUID()

    let firstName: String
    let lastName: String
    let phoneNumber: String
    let address: String
    let photo: String
}

var contacts: [Contact] = [
    Contact(firstName: "Rhea",
            lastName: "Poole",
            phoneNumber: "+12 345 567 890",
            address: "Sunbay 2173",
            photo: "photo"),
    Contact(firstName: "Seren",
        lastName: "Murillo",
        phoneNumber: "+12 345 567 890",
        address: "Sunbay 2173",
        photo: "photo2"),
    Contact(firstName: "Ivor",
        lastName: "Adams",
        phoneNumber: "+12 345 567 890",
        address: "Sunbay 2173",
        photo: "photo3"),
    Contact(firstName: "Rhea",
            lastName: "Poole",
            phoneNumber: "+12 345 567 890",
            address: "Sunbay 2173",
            photo: "photo"),
    Contact(firstName: "Seren",
        lastName: "Murillo",
        phoneNumber: "+12 345 567 890",
        address: "Sunbay 2173",
        photo: "photo2"),
    Contact(firstName: "Ivor",
        lastName: "Adams",
        phoneNumber: "+12 345 567 890",
        address: "Sunbay 2173",
        photo: "photo3")
]
