//
//  ContactDetailView.swift
//  Example-SwiftUI-Finished
//
//  Created by aleksander.lorenc on 30/10/2019.
//  Copyright © 2019 Unwrapped Software. All rights reserved.
//

import SwiftUI

struct ContactDetailView: View {
    private let contact: Contact

    init(contact: Contact) {
        self.contact = contact
    }

    var body: some View {
        Image(contact.photo)
            .resizable()
            .scaledToFit()
            .frame(width: nil, height: 400)
    }
}

struct ContactDetailView_Previews: PreviewProvider {
    static var previews: some View {
        ContactDetailView(contact: contacts[0])
    }
}
