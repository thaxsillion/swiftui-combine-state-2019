//
//  ContentView.swift
//  Example-SwiftUI-Finished
//
//  Created by aleksander.lorenc on 30/10/2019.
//  Copyright © 2019 Unwrapped Software. All rights reserved.
//

import SwiftUI

struct ContactView: View {
    private let contact: Contact

    init(contact: Contact) {
        self.contact = contact
    }

    var body: some View {
        HStack(alignment: .top) {
            Image(contact.photo)
                .resizable()
                .frame(width: 50, height: 50)

            VStack(alignment: .leading) {
                Text("\(contact.firstName) \(contact.lastName)")
                    .font(.body)
                Text(contact.address)
                    .font(.subheadline)
                    .fontWeight(.light)
                    .foregroundColor(Color(UIColor.darkGray))
            }

            Spacer()

            Text(contact.phoneNumber)
                .font(.subheadline)
                .foregroundColor(Color(UIColor.darkGray))
        }
    }
}

struct ContentView: View {
    private let contacts: [Contact]

    init(contacts: [Contact]) {
        self.contacts = contacts
    }
    var body: some View {
        NavigationView {
            List(contacts) { contact in
                NavigationLink(destination: ContactDetailView(contact: contact)) {
                    ContactView(contact: contact)
                }
            }.navigationBarTitle("My contacts")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(contacts: contacts)
    }
}
