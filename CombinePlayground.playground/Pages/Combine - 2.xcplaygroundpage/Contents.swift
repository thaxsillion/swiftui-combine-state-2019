import UIKit
import Combine

let x = Array(repeating: 0, count: 100).map { _ in Int.random(in: 111...999) }

let reference = Publishers.Sequence<[Int], URLError>(sequence: x)
    .map { $0 * 3 }
    .filter { $0.isMultiple(of: 2) }
    .prefix(3)
    .flatMap { number in
        URLSession.shared.dataTaskPublisher(for: URL(string: "http://www.someapi.com/\(number)")!)
    }
    .sink(receiveCompletion: { _ in
        print("error")
    }, receiveValue: { data, response in
        guard let response = String(data: data, encoding: .utf8) else { return }
        print(response)
    })


