
import UIKit
import Combine

let x = Array(repeating: 0, count: 100).map { _ in Int.random(in: 111...999) }

Publishers.Sequence(sequence: x)
    .map { $0 * 3 }
    .filter { $0.isMultiple(of: 2) }
    .sink { number in
        print(number)
    }
