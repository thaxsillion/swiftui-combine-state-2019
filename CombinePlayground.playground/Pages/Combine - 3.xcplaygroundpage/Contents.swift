//: [Previous](@previous)

import Foundation
import Combine

struct IPInfo: Codable {
    // matching the data structure returned from ip.jsontest.com
    var ip: String
}

let myURL = URL(string: "http://ip.jsontest.com")
// NOTE(heckj): you'll need to enable insecure downloads in your Info.plist for this example
// since the URL scheme is 'http'

let remoteDataPublisher = URLSession.shared.dataTaskPublisher(for: myURL!)
    // the dataTaskPublisher output combination is (data: Data, response: URLResponse)
    .map({ (inputTuple) -> Data in
        return inputTuple.data
    })
    .decode(type: IPInfo.self, decoder: JSONDecoder())
    .catch { err in
        return Just(IPInfo(ip: "8.8.8.8"))
    }
    .eraseToAnyPublisher()
    .sink { value in
        print(value.ip)
    }
