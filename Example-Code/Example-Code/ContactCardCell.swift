//
//  ContactCardCell.swift
//  Example-Code
//
//  Created by aleksander.lorenc on 30/10/2019.
//  Copyright © 2019 Unwrapped Software. All rights reserved.
//

import UIKit

class ContactCardCell: UITableViewCell {
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 17)
        return label
    }()

    private lazy var addressLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: .light)
        label.textColor = .darkGray
        return label
    }()

    private lazy var phoneNumberLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor(white: 60 / 255.0, alpha: 1.0)
        return label
    }()

    private lazy var photoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addViews()
        configureViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func addViews() {
        addSubview(nameLabel)
        addSubview(addressLabel)
        addSubview(phoneNumberLabel)
        addSubview(photoImageView)
    }

    private func configureViews() {
        let photoImageViewBottomAnchor = photoImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16)
        photoImageViewBottomAnchor.priority = .defaultHigh

        let constraints = [
            photoImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 16),
            photoImageView.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            photoImageView.heightAnchor.constraint(equalToConstant: 50),
            photoImageView.widthAnchor.constraint(equalTo: photoImageView.heightAnchor),
            photoImageViewBottomAnchor,
            nameLabel.topAnchor.constraint(equalTo: photoImageView.topAnchor),
            nameLabel.leftAnchor.constraint(equalTo: photoImageView.rightAnchor, constant: 16),
            addressLabel.leftAnchor.constraint(equalTo: nameLabel.leftAnchor),
            addressLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 4),
            phoneNumberLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -16),
            phoneNumberLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16)
        ]

        constraints.forEach { $0.isActive = true }
    }

    func configure(name: String,
                   address: String,
                   phoneNumber: String,
                   photo: UIImage) {
        nameLabel.text = name
        addressLabel.text = address
        phoneNumberLabel.text = phoneNumber
        photoImageView.image = photo
    }
}
